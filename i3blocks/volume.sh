#!/bin/bash

vol="$(pamixer --get-volume)"

mute="$(pamixer --get-mute)"

if [ "$mute" == "true" ]
then
echo "muted"
echo
echo \#9B1003
else
echo " "$vol
echo
echo \#48AAAD
fi
