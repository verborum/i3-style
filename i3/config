set $mod Mod4

font pango:DejaVu Sans Mono 12

floating_modifier $mod

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec rofi -show run

bindsym $mod+Escape exec poweroff 

bindsym XF86AudioRaiseVolume exec --no-startup-id pamixer -i 5; exec pkill -SIGRTMIN+10 i3blocks
bindsym XF86AudioLowerVolume exec --no-startup-id pamixer -d 5; exec pkill -SIGRTMIN+10 i3blocks
bindsym XF86AudioMute exec --no-startup-id ~/.config/i3/mute.sh; exec pkill -SIGRTMIN+10 i3blocks

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

set $m1 "1:  "
set $m2 "2:  "
set $m3 "3:  "
set $m4 "4"
set $m5 "5:  "
set $m6 "6"
set $m7 "7:  "
set $m8 "8"
set $m9 "9"
set $m10 "10"

# switch to workspace
bindsym $mod+1 workspace $m1
bindsym $mod+2 workspace $m2
bindsym $mod+3 workspace $m3
bindsym $mod+4 workspace $m4
bindsym $mod+5 workspace $m5
bindsym $mod+6 workspace $m6
bindsym $mod+7 workspace $m7
bindsym $mod+8 workspace $m8
bindsym $mod+9 workspace $m9
bindsym $mod+0 workspace $m10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $m1
bindsym $mod+Shift+2 move container to workspace $m2
bindsym $mod+Shift+3 move container to workspace $m3
bindsym $mod+Shift+4 move container to workspace $m4
bindsym $mod+Shift+5 move container to workspace $m5
bindsym $mod+Shift+6 move container to workspace $m6
bindsym $mod+Shift+7 move container to workspace $m7
bindsym $mod+Shift+8 move container to workspace $m8
bindsym $mod+Shift+9 move container to workspace $m9
bindsym $mod+Shift+0 move container to workspace $m10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Autostart
exec nm-applet

# Screenshots
bindsym Print exec --no-startup-id maim "/home/ubersenpaii/Pictures/$(date)"
bindsym $mod+Print exec --no-startup-id maim --window $(xdotool getactivewindow) "/home/ubersenpaii/Pictures/$(date)"
bindsym Shift+Print exec --no-startup-id maim --select "/home/ubersenpaii/Pictures/$(date)"

## Clipboard Screenshots
bindsym Ctrl+Print exec --no-startup-id maim | xclip -selection clipboard -t image/png
bindsym Ctrl+$mod+Print exec --no-startup-id maim --window $(xdotool getactivewindow) | xclip -selection clipboard -t image/png
bindsym Ctrl+Shift+Print exec --no-startup-id maim --select | xclip -selection clipboard -t image/png

# switch language
exec_always "setxkbmap -model pc104 -layout us,ru -variant ,, -option grp:alt_shift_toggle"

# set wallpapers
exec feh --bg-scale ~/Images/wallpaper.jpg

exec picom --experimental-backends --backend glx

bindsym $mod+plus exec i3-msg gaps inner current plus 5
bindsym $mod+minus exec i3-msg gaps inner current minus 5

default_border pixel 2
default_floating_border pixel 1


# class                 border  bground text    indicator child_border
client.focused #0088CC #0088CC #ffffff #dddddd
client.focused_inactive #333333 #333333 #888888 #292d2e
client.unfocused #333333 #333333 #888888 #292d2e
client.urgent #2f343a #900000 #ffffff #900000
client.placeholder      #000000 #0C0C0C #FFFFFF #000000   #0C0C0C

client.background       #FFFFFF

bar {

  position top

  status_command i3blocks -c  ~/.config/i3blocks/config

  colors {
    background #222222
    statusline #dddddd
    separator #666666

    focused_workspace #0088CC #0088CC #ffffff #FFFFFF
    active_workspace #333333 #333333 #ffffff #FFFFFF
    inactive_workspace #333333 #333333 #888888 #888888
    urgent_workspace #2f343a #900000 #ffffff #FFFFFF
  }
}

